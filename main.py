import secrets
from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel

# uvicorn main:app --reload

app = FastAPI()

origins = [
    "http://localhost:8080",
    "http://localhost:8080/auth"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

users_auth = {'Roman': {'password': '123456', 'isAdmin': True, 'id': 'abcd0', 'boards': ['a112', 'a221']},
              'Dmitriy': {'password': '123456', 'isAdmin': False, 'id': 'abcd1', 'boards': ['a112', 'a221']},
              'Vladimir': {'password': '123456', 'isAdmin': False, 'id': 'abcd2', 'boards': ['a112', 'a221']},
              'Igor': {'password': '123456', 'isAdmin': False, 'id': 'abcd3', 'boards': ['a112']}}

boards = {'a112': {'users': ['abcd0', 'abcd1', 'abcd2', 'abcd3'], 'tasks': {
    '0': {'id': 0, 'title': 'Сделать Markdown0', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'Done', 'assigned': 'abcd0', 'reviewer': 'abcd2',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '1': {'id': 1, 'title': 'Сделать Markdown1', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'Review', 'assigned': '', 'reviewer': 'abcd2',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '2': {'id': 2, 'title': 'Сделать Markdown2', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd1', 'reviewer': 'abcd2',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '3': {'id': 3, 'title': 'Сделать Markdown3', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'Test', 'assigned': 'abcd2', 'reviewer': 'abcd1',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '4': {'id': 4, 'title': 'Сделать Markdown4', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd3', 'reviewer': '',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '5': {'id': 5, 'title': 'Сделать Markdown5', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'Done', 'assigned': 'abcd0', 'reviewer': 'abcd2',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '6': {'id': 6, 'title': 'Сделать Markdown6', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'Review', 'assigned': '', 'reviewer': 'abcd2',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '7': {'id': 7, 'title': 'Сделать Markdown7', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd1', 'reviewer': 'abcd2',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '8': {'id': 8, 'title': 'Сделать Markdown8', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'Test', 'assigned': 'abcd2', 'reviewer': 'abcd1',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '9': {'id': 9, 'title': 'Сделать Markdown9', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd3', 'reviewer': '',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '10': {'id': 10, 'title': 'Сделать Markdown10', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'Done', 'assigned': 'abcd0', 'reviewer': 'abcd2',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '11': {'id': 11, 'title': 'Сделать Markdown11', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'Review', 'assigned': '', 'reviewer': 'abcd2',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '12': {'id': 12, 'title': 'Сделать Markdown12', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd1', 'reviewer': 'abcd2',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '13': {'id': 13, 'title': 'Сделать Markdown13', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'Test', 'assigned': 'abcd2', 'reviewer': 'abcd1',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '14': {'id': 14, 'title': 'Сделать Markdown14', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd3', 'reviewer': '',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '15': {'id': 15, 'title': 'Сделать Markdown15', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'Done', 'assigned': 'abcd0', 'reviewer': 'abcd2',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '16': {'id': 16, 'title': 'Сделать Markdown16', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'Review', 'assigned': '', 'reviewer': 'abcd2',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '17': {'id': 17, 'title': 'Сделать Markdown17', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd1', 'reviewer': 'abcd2',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '18': {'id': 18, 'title': 'Сделать Markdown18', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'Test', 'assigned': 'abcd2', 'reviewer': 'abcd1',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '19': {'id': 19, 'title': 'Сделать Markdown19', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd3', 'reviewer': '',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '20': {'id': 20, 'title': 'Сделать Markdown20', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'Done', 'assigned': 'abcd0', 'reviewer': 'abcd2',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '21': {'id': 21, 'title': 'Сделать Markdown21', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'Review', 'assigned': '', 'reviewer': 'abcd2',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '22': {'id': 22, 'title': 'Сделать Markdown22', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd1', 'reviewer': 'abcd2',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '23': {'id': 23, 'title': 'Сделать Markdown23', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'Test', 'assigned': 'abcd2', 'reviewer': 'abcd1',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '24': {'id': 24, 'title': 'Сделать Markdown24', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd3', 'reviewer': '',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
}}, 'a221': {'users': ['abcd0', 'abcd1', 'abcd2'], 'tasks': {
    '0': {'id': 0, 'title': 'Сделать Markdown0', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'Done', 'assigned': 'abcd0', 'reviewer': 'abcd2',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '1': {'id': 1, 'title': 'Сделать Markdown1', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'Review', 'assigned': '', 'reviewer': 'abcd2',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '2': {'id': 2, 'title': 'Сделать Markdown2', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd1', 'reviewer': 'abcd2',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '3': {'id': 3, 'title': 'Сделать Markdown3', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'Test', 'assigned': 'abcd2', 'reviewer': 'abcd1',
          'body': 'Верхняя задача. Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '4': {'id': 4, 'title': 'Сделать Markdown4', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd3', 'reviewer': '',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '5': {'id': 5, 'title': 'Сделать Markdown5', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'Done', 'assigned': 'abcd0', 'reviewer': 'abcd2',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '6': {'id': 6, 'title': 'Сделать Markdown6', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'Review', 'assigned': '', 'reviewer': 'abcd2',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '7': {'id': 7, 'title': 'Сделать Markdown7', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd1', 'reviewer': 'abcd2',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '8': {'id': 8, 'title': 'Сделать Markdown8', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'Test', 'assigned': 'abcd2', 'reviewer': 'abcd1',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '9': {'id': 9, 'title': 'Сделать Markdown9', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
          'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd3', 'reviewer': '',
          'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '10': {'id': 10, 'title': 'Сделать Markdown10', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'Done', 'assigned': 'abcd0', 'reviewer': 'abcd2',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '11': {'id': 11, 'title': 'Сделать Markdown11', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'Review', 'assigned': '', 'reviewer': 'abcd2',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '12': {'id': 12, 'title': 'Сделать Markdown12', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd1', 'reviewer': 'abcd2',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '13': {'id': 13, 'title': 'Сделать Markdown13', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'Test', 'assigned': 'abcd2', 'reviewer': 'abcd1',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '14': {'id': 14, 'title': 'Сделать Markdown14', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd3', 'reviewer': '',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '15': {'id': 15, 'title': 'Сделать Markdown15', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'Done', 'assigned': 'abcd0', 'reviewer': 'abcd2',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '16': {'id': 16, 'title': 'Сделать Markdown16', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'Review', 'assigned': '', 'reviewer': 'abcd2',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '17': {'id': 17, 'title': 'Сделать Markdown17', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'In work', 'assigned': 'abcd1', 'reviewer': 'abcd2',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    '18': {'id': 18, 'title': 'Сделать Markdown18', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
           'storyPoints': 2, 'status': 'Test', 'assigned': 'abcd2', 'reviewer': 'abcd1',
           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
}}}

users = {'abcd0': {'isAdmin': True, 'name': 'Roman', 'gitLink': 'https://gitlab.com/'},
         'abcd1': {'isAdmin': False, 'name': 'Dmitriy', 'gitLink': 'https://gitlab.com/'},
         'abcd2': {'isAdmin': False, 'name': 'Vladimir', 'gitLink': 'https://gitlab.com/'},
         'abcd3': {'isAdmin': False, 'name': 'Igor', 'gitLink': 'https://gitlab.com/'}}

tasks_preview = [
    {'id': 0, 'title': 'Сделать Markdown0', 'date': 1623589536, 'authorId': 'abcd0', 'deadLine': 1623595584,
     'storyPoints': 2, 'status': 'Done',
     'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    {'id': 1, 'title': 'Сделать Markdown1', 'date': 1623589536, 'authorId': 'abcd0', 'deadLine': 1623595584,
     'storyPoints': 2, 'status': 'Review',
     'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    {'id': 2, 'title': 'Сделать Markdown2', 'date': 1623589536, 'authorId': 'abcd0', 'deadLine': 1623595584,
     'storyPoints': 2, 'status': 'In work',
     'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    {'id': 3, 'title': 'Сделать Markdown3', 'date': 1623589536, 'authorId': 'abcd0', 'deadLine': 1623595584,
     'storyPoints': 2, 'status': 'Test',
     'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
    {'id': 4, 'title': 'Сделать Markdown4', 'date': 1623589536, 'authorId': 'abcd0', 'deadLine': 1623595584,
     'storyPoints': 2, 'status': 'In work',
     'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},
]


@app.get("/")
async def root():
    return users


# @app.post("/users")
# async def getUsers(users):
#     return users


@app.get("/board/{board_id}")
async def read_board(board_id):
    return {'tasks': boards[board_id]['tasks'], 'users': getUsersById(boards[board_id]['users'])}


def getUsersById(idList):
    boardUsers = []
    for i in idList:
        boardUsers.append({'id': i, 'userInfo': users[i]})
    return boardUsers


def find_user_info(board_id, id):
    reviewer = boards[board_id]['tasks'][id]['reviewer']
    if reviewer in users.keys():
        reviewer = {'name': users[reviewer]['name'], 'gitLink': users[reviewer]['gitLink']}
    assigned = boards[board_id]['tasks'][id]['assigned']
    if assigned in users.keys():
        assigned = {'name': users[assigned]['name'], 'gitLink': users[assigned]['gitLink']}
    author = boards[board_id]['tasks'][id]['author']
    if author in users.keys():
        author = {'name': users[author]['name'], 'gitLink': users[author]['gitLink']}
    return {'reviewer': reviewer, 'assigned': assigned, 'author': author, 'task': boards[board_id]['tasks'][id]}


@app.get("/board/{board_id}/{task_id}")
async def task_info(board_id, task_id):
    return find_user_info(board_id, task_id)


def change_task_items(user, board_id, task_id):
    boards[board_id]["tasks"][task_id]["status"] = user.status
    return {'boards': boards, 'board': {'board': boards[board_id], 'users': getUsersById(boards[board_id]['users'])}}


def change_task_info(user, board_id, task_id):
    if user.status:
        return change_task_items(user, board_id, task_id)
    else:
        info = find_user_info(board_id, task_id)
        info[user.role] = user.user["userInfo"]
        boards[board_id]["tasks"][task_id][user.role] = user.user["id"]
    # print()
    # boards = {'a112': {'users': ['abcd0', 'abcd1', 'abcd2', 'abcd3'], 'tasks': {
    #     '0': {'id': 0, 'title': 'Сделать Markdown0', 'date': 1623589536, 'author': 'abcd0', 'deadLine': 1623595584,
    #           'storyPoints': 2, 'status': 'Done', 'assigned': 'abcd0', 'reviewer': 'abcd2',
    #           'body': 'Почитать специализацию markdown, саму либу и реализовать парсинг для отправки сообщения'},

    return info


class User(BaseModel):
    role: str = None
    user: dict = None
    status: str = None


@app.post("/board/{board_id}/{task_id}")
async def task_info(board_id, task_id, user: User):
    return change_task_info(user, board_id, task_id)


# task = None, id = None
def change_task(body):
    if body.task:
        boards[body.board_id]['tasks'][body.id] = body.task
        return boards[body.board_id]['tasks']


@app.post("/board/{board_id}")
async def change_task(body, board_id):
    return boards.board_id.tasks


@app.get("/landing")
async def read_board_preview():
    return tasks_preview


security = HTTPBasic()


def get_current_username(credentials: HTTPBasicCredentials = Depends(security)):
    user = users_auth[credentials.username]
    correct_password = secrets.compare_digest(credentials.password, user['password'])
    if not correct_password:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    return {'username': credentials.username, 'id': user['id'], 'boards': user['boards']}


@app.get("/auth")
def auth(user=Depends(get_current_username)):
    return {"username": user['username'], 'id': user['id'], 'boards': user['boards']}
